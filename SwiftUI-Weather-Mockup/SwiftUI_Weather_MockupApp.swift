//
//  SwiftUI_Weather_MockupApp.swift
//  SwiftUI-Weather-Mockup
//
//  Created by Tofik Khan on 4/30/21.
//

import SwiftUI

@main
struct SwiftUI_Weather_MockupApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
