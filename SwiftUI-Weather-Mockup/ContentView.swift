//
//  ContentView.swift
//  SwiftUI-Weather-Mockup
//
//  Created by Tofik Khan on 4/30/21.
//

/*
    Weather UI Mockup
        A mockup of a weather app that changes apperence on the click
        of the UI button
 
    Inspiration
        The idea for this is taken from Sean Allen's YouTube video.
 */

import SwiftUI

struct ContentView: View {
    
    //state variable that updates to true for DarkMode UI
    @State private var isDarkMode = false
    
    var body: some View {
        ZStack {
            BackgroundView(isDarkMode: $isDarkMode)
            VStack {
                CityTextView(cityName: "Vancouver, WA")
                todayWeatherView(imageName: "cloud.sun.fill",
                                 temprature: 76)
                
                HStack (spacing: 25) {
                    //WeekWeatherInfoContainer
                    DayWeatherView(dayOfWeek: "SAT",
                                   iconImg: "cloud.rain.fill",
                                   temprature: 63)
                    
                    DayWeatherView(dayOfWeek: "SUN",
                                   iconImg: "cloud.sun.fill",
                                   temprature: 66)
                    
                    DayWeatherView(dayOfWeek: "MON",
                                   iconImg: "cloud.fill",
                                   temprature: 61)
                    
                    DayWeatherView(dayOfWeek: "TUE",
                                   iconImg: "cloud.sun.fill",
                                   temprature: 66)
                    
                    DayWeatherView(dayOfWeek: "WED",
                                   iconImg: "cloud.fill",
                                   temprature: 81)
                }
                
                Spacer()
                
                Button {
                    isDarkMode.toggle()
                } label: {
                    WeatherButton(text: "Change Day Time",
                                   textColor: .blue,
                                   backgroundColor: .white)
                }
                
                Spacer()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct BackgroundView: View {
    
    @Binding var isDarkMode: Bool

    var body: some View {
        LinearGradient(gradient: Gradient(
                        colors: [isDarkMode ? .black : .blue,
                                 isDarkMode ? .gray : Color("lightblue")]),
                                        startPoint: .topLeading,
                                        endPoint: .bottomTrailing)
            .edgesIgnoringSafeArea(.all)
    }
}

struct CityTextView: View {
    var cityName: String
    var body: some View {
        Text(cityName)
            .font(.system(size: 32,
                          weight: .medium,
                          design: .default))
            .foregroundColor(.white)
            .padding()
    }
}

struct todayWeatherView: View {
    var imageName: String
    var temprature: Int
    var body: some View {
        VStack (spacing: 10) {
            //MainWeatherInfo Container
            Image(systemName: imageName)
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 180,
                       height: 180)
            
            Text("\(temprature)°")
                .font(.system(size: 70, weight: .medium))
                .foregroundColor(.white)
        }
        .padding(.bottom, 50)
    }
}

struct DayWeatherView: View {
    
    var dayOfWeek: String
    var iconImg: String
    var temprature: Int
    
    var body: some View {
        VStack {
            Text(dayOfWeek)
                .font(.system(size: 16,
                              weight: .medium,
                              design: .default))
                .foregroundColor(.white)
            Image(systemName: iconImg)
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 50,
                       height: 50,
                       alignment: .top)
            Text("\(temprature)°")
                .font(.system(size: 30,
                              weight: .medium))
                .foregroundColor(.white)
        }
    }
}

struct WeatherButton: View {
    
    var text: String
    var textColor: Color
    var backgroundColor: Color
    
    var body: some View {
        Text(text)
            .frame(width: 280,
                   height: 50)
            .background(backgroundColor)
            .foregroundColor(textColor)
            .font(.system(size: 20,
                          weight: .bold,
                          design: .default))
            .cornerRadius(10)
    }
}
